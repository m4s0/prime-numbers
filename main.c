#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool check_is_prime(int number);

int main(int argc, const char *argv[]) {
    FILE *file = fopen(argv[1], "r");
    char line[1024];

    int number;
    bool first_occurrence;
    while (fgets(line, 1024, file)) {
        number = atoi(line);
        first_occurrence = true;
        for (int current_number = 2; current_number <= number; current_number++) {
            if (check_is_prime(current_number)) {
                if (!first_occurrence)
                    printf(",");
                printf("%d", current_number);
                first_occurrence = false;
            }
        }

        printf("\n");
    }
    return 0;
}

bool check_is_prime(int number) {
    bool is_prime = true;

    if (number < 1)
        return false;

    if (number > 2) {
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                is_prime = false;
                break;
            }
        }
        if (!is_prime)
            return false;
    }

    return true;
}
